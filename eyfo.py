'''

EYFO - a local file indexer

contact:    https://bitbucket.org/yodeleyihu/eyfo
license:    LGPL
created:    2013-02-03
version:    0.4 (alpha)

This will need to be heavily reimplemented.

It was originally created by Boa Constructor, but is not compatible with it anymore, due to the use of "file_list" object. It would be nice to use Boa again, I just need to understand how to add file_list as a user-control...

Eyfo's constitution:
    - responsiveness - speed IS an issue
    - accessibility - the user interface must support keyboard shortcuts and shortcuts.
    - multi-platform
    - open source
    - multi-lingual:  detect and convert file's encoding, and convert its contents to unicode (utf8). I don't care much about the UI language, btw.
    - easy to read, write, maintain, and add plugins
    - plugins:  support extracting semantically meaningful parts of documents from as many file formats as needed.
    -

'''

import library
from os.path import expanduser, isdir, isfile, join, basename, abspath, splitext
from persistant_dict import persistant_dict
from wx import MessageBox as msgbox
from wx.lib.anchors import LayoutAnchors
import filelist
import os
import whoosh_indexer
import wx
import wx.animate
import wx.html
import wx.html2
import eyfoview

from eyfo_defaults import default_config_values, CONF_FILE_BASE_NAME



class search_form_controller(eyfoview.search_form):

    def init_eyfo(self):

        if conf['index_db_dir']:
            d = conf['index_db_dir']
            if not isdir(d): d = "d:\\temp\\whoosh"   # xxx todo: find a better directory...
            whoosh_indexer.index_db_dir = d

        if conf['root_folder']:
            d = conf['root_folder']
            if not isdir(d): d = "c:\\"   # xxx todo: find a better directory...
            whoosh_indexer.root_folder = d

        if conf['windowleft'] and conf['windowtop']:
            self.SetPosition( (conf['windowleft'], conf['windowtop']) )

        if conf['windowwidth'] and conf['windowheight']:
            self.SetSize( (conf['windowwidth'], conf['windowheight']) )

        if conf['maximized']:
            self.Maximize()

        if conf['max_results']:
            eyfoview.MAX_RESULTS = conf['max_results']

        if conf['exclude_patterns']:
            eyfoview.exclude_patterns = conf['exclude_patterns']

        for combo in [ self.search_words_combo, self.dont_search_words_combo ]:
            items = conf[ combo.Name ]
            if items and type(items) is list:
                combo.Items = conf[ combo.Name ]
                combo.SetSelection(0)  # not a good idea for reject list...?

        if conf['progress_indicator_gif_animation_file']:
            f = conf['progress_indicator_gif_animation_file']
            if isfile(f):
                self.progress_indicator.LoadFile( f )


    def get_list_file(self, wxlist):
        i = self.file_list.GetFocusedItem()
        f = self.file_list.GetItem(i,0).Text
        p = self.file_list.GetItem(i,1).Text
        f=join(p,f)
        if not isfile(f):
            msgbox("file <%s> is missing" % f)
            return ""
        else:
            return f

    def __init__(self):

        parent = None
        self.conf = conf
        self._init_ctrls(parent)
        self.init_eyfo()

    def OnIndex_buttonButton(self, event):
        msg = "indexer is not fully implemented in the GUI.\n You'll get more from the command line\n python whoosh_indexer.py for usage info."
        print msg
        msgbox(msg)

        d = wx.MessageDialog(self, u"IT IS GOING TO FREEZE THE GUI FOR AN HOUR OR SO.\n\n OK ? ", "alpha state")
        ans = d.ShowModal()
        if not ans in [wx.OK, wx.YES, wx.ID_YES, wx.ID_OK]:
            return

        self.progress_indicator.Play()
        root = conf['root_folder']
        i = whoosh_indexer.Eyfo_index( conf['index_db_dir'], clean=False )
        i.only_fnames = False  #conf['index_names_only']
        i.incremental_index( dirname=root )
        self.progress_indicator.Stop()


    def OnExit_buttonButton(self, event):
        self.Close()

    def OnSearch_buttonButton(self, event):
        self.progress_indicator.Play()
        wx.GetApp().Yield(True)   # lets start the progress indicator

        self.file_list.DeleteAllItems() # .ClearAll()
        s = whoosh_indexer.Eyfo_Search( conf['index_db_dir'] )
        # todo
        s.only_fnames = (self.where_to_search=='Search &File names only')
        query = self.search_words_combo.Value
        txt = s.search( query, gui=True )   #???
        files = txt.splitlines()
        if not files:
            self.file_list.add_file(' { nothing found } ')
            return 0

        self.statusbar.SetStatusText(number=0, text=u'%d indexed files' % len(files))
        for f in files:
            if isfile(f):
                wx.GetApp().Yield(True)   # i'd rather do it as a background thread, e.g. sendmessage but i don't know how. this list can have thousands of items in it. currently it's limited to max-something. there's an interesting answer by robin in the wxpython group.
                self.file_list.add_file(f)

        self.file_list.auto_width()

        self.update_history( self.search_words_combo, query )
        #self.update_history( self.dont_search_words_combo, query )

        #self.file_list.SetFocus()
        self.progress_indicator.Stop()

    def update_history( self, combo, text ):
        text = text.strip()
        if not text: return

        items = combo.Items
        if text in items:
            items.remove(text)
        items.insert(0, text)
        combo.Items = items
        combo.SetSelection(0)
        conf[combo.Name] = items


    def OnFile_listKeyUp(self, event):
        f = self.get_list_file(self.file_list)

        key = event.GetUnicodeKey()
        if key==13:
            self.OnFile_listLeftDclick(None)

        key = event.GetKeyCode()
        # handle F4
        if key == wx.WXK_F4:
            editor = ''
            if 'editor' in conf:
                editor = conf['editor']
            if not isfile(editor) and 'editor' in os.environ:
                editor = os.environ['editor']
            if not isfile(editor):
                msgbox("Editor is not defined (as env-var or in eyfo-config.json)")
                return
            library.run('%s "%s"' % (editor, f), wait=False)


    def OnFile_listLeftDclick(self, event):
        f = self.get_list_file(self.file_list)
        if f:
            library.open_any_file(f)

    def OnFile_listListItemRightClick(self, event):
        f = self.get_list_file(self.file_list)
        folder = os.path.split(f)[0]
        conf['filesystem_manager']
        library.run('"%s" "%s"' % (conf['filesystem_manager'], folder), wait=False )


    def OnFile_listListItemSelected(self, event):
        f = self.get_list_file(self.file_list)
        if f:
            #self.html.LoadURL(f)
            ext = os.path.splitext(f)[1][1:]
            if ext in ['jpg', 'jpeg','gif','png','bmp','ico','wmf','svg','tif','tiff','pnm','apng',
                        'avi', 'mpg','mpeg','mp4','mp3','wav','ogg','svg','tif','tiff','pnm','apng',]:
                html = '<embed src="%s" width="100%%" height="100%%" />' % f.replace('\\','/')
                #self.html.PageSource(html)
                #self.html.SetPage(html)
                self.html.LoadFile(f)
            elif ext in ['txt', 'ini','py','inf','aspx','html','htm','php','asp','aspx',]:
                self.html.LoadFile(f)
            elif ext in ['pdf', 'swf','ttf',]:
                pass
                self.SetLabel("zorba")
            else:
                print 'else', ext
                self.html.LoadPage("blank.html")

    def OnAdd_path_buttonButton(self, event):
        event.Skip()

    def OnDel_path_buttonButton(self, event):
        event.Skip()

    def OnSelect_all_path_buttonButton(self, event):
        event.Skip()

    def get_list_file(self, wxlist):
        i = self.file_list.GetFocusedItem()
        f = self.file_list.GetItem(i,0).Text
        p = self.file_list.GetItem(i,1).Text
        f=join(p,f)
        if not isfile(f):
            msgbox("file <%s> is missing" % f)
            return ""
        else:
            return f

    def keybaord_handler(self, event):
        key = event.GetKeyCode()
        if key == wx.WXK_ESCAPE:
            self.Close()
            return
        event.Skip()

    def order_tab_key(self):
        #self.order_tab_key
        order = (
            self.search_words_combo,
            self.search_button,
            self.dont_search_words_combo,
            self.exit_button,
            self.where_to_search,
            self.index_button,
            self.add_path_button,
            self.del_path_button,
            self.select_all_path_button,
        )
        for i in xrange(len(order) - 1):
            order[i+1].MoveAfterInTabOrder(order[i])

    def OnDialog1Maximize(self, event):
        conf['maximized'] = True
        event.Skip()

    def OnDialog1Move(self, event):
        x,y = self.GetPosition()
        conf['windowleft'] = x
        conf['windowtop'] = y
        event.Skip()

    def OnDialog1Size(self, event):
        conf['maximized'] = False
        w,h = self.GetSize()
        conf['windowwidth'] = w
        conf['windowheight'] = h
        event.Skip()




configfile = library.find_user_dir( particular_file=CONF_FILE_BASE_NAME, return_file_or_home='file', accept_current_dir=True, create_if_not_exist=True )
print 'configfile',configfile
conf = persistant_dict( configfile )
conf.update_missing( default_config_values )

if __name__ == '__main__':

    app = wx.App()
    dlg = search_form_controller()
    try:
        dlg.ShowModal()
    finally:
        dlg.Destroy()
    app.MainLoop()
