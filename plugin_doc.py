#import file_types
#def archive(filename, temp_dir)
#from office_lib import docx2text
from library import read_stdout_from_cmd, encode_command_arguments, decode_command_arguments
from library import html2text
from library import get_html_headers

antiword_home = r'%progs%\programs\antiword'
wv = r'%progs%\wv\bin\wvWare.exe'

extensions = ['doc', 'wbk',  ]

''' requires antiword properly installed :
        set HOME=<sometong>
        antiword in %HOME%\bin\
        utf-8.txt in %HOME%\share\antiword\
        call antiword.exe with FULL path !
    '''

def get_contents(f, data):
    #try:
    cmd = (wv, '--nographics', f)
    cmd = encode_command_arguments(cmd)
    env = {'HOME': 'antiword_home' }
    data.body = read_stdout_from_cmd( cmd, additional_env=env)
    data.body = unicode(data.body, 'utf-8', 'replace')
    data.titles = get_html_headers(data.body, flat=True)
    data.body = html2text(data.body, brutal=True)
    #except:
    #    print 'error in DOC file?'
    return data

