#!/usr/bin/env python
'''

EYFO - a local file indexer

contact:    https://bitbucket.org/yodeleyihu/eyfo
license:    LGPL
created:    2013-02-03
version:    0.4 (alpha)

not implemented.

intention:
    hooks to filesystem events (create, delete, update, rename)
    add to some funny queue
    when user is inactive (by I/O? psutils?) feed the queue to whoosh indexer

'''



import time
import sys, os
import re
from os.path import expanduser, isdir, isfile, join, basename, abspath, splitext
import subprocess
#from configobj import ConfigObj
from exceptions import AttributeError
import os
import sys
import re
import string
from os.path import expanduser, isdir, isfile, join, abspath, basename
from os import walk
from shutil import copyfile
from time import time as currtime
from time import sleep
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter as formatter
import urllib2
from library import log, now

times=[]
times.append((currtime(), 'Start'))

try:
    pass

except:
    pass


class C:

    def __init__(self, boxstr=None ):
        pass

    def __repr__(self):
        return "hello %s" % ('', )

    def f(self, x):
        return



def process_cmd_line_args(argv):
    desc= ''' hello! '''
    epilog = ''' goodbye. '''

    parser = ArgumentParser( formatter_class=formatter, description=desc, epilog=epilog )
    parser.add_argument("--action", dest="action", default='', help=actions_help_short)
    parser.add_argument("--config-file", dest="config_file", default='face.ini', help="use specific config file")
    parser.add_argument("--image", dest="image_file", default='', help='a plain text file with a image file name in each line')
    parser.add_argument("--image-list-file", dest="image_list_file", default=None, help='a plain text file with a image file name in each line. you can create the list with: find /path/pics/ -type f -name \'*.jpg\' > /path/filelist.txt')
    parser.add_argument("--log-file", dest="log_file", default='face_recognition.log', help="use specific log file")
    parser.add_argument('--mark', dest='mark', action='store_true', help='draw a rectangle on the found face')
    parser.add_argument('--results-file', dest='results_file', help='provide specific "labels.xml" file, to use for confusion matrix')
    parser.add_argument("--root-dir", dest="root_dir", default='', help='root folder for importing images from')
    parser.add_argument("--silent", action='store_true', dest='silent', default=False, help="don't print progress details")
    parser.add_argument("--source-type", dest="image_source_type", default='', help='where did these iamges came from?  '+source_types_str)
    parser.add_argument('--testing-file', dest='testing_file', default='', help='override testing data - read from existing file')
    parser.add_argument('--timestamp', dest='timestamp', default='', help='optionally execute the action with files which were created at specific timestamp')


    results = parser.parse_args()
    args = dict(results._get_kwargs())

    if not args['config_file']:
        log( "config filename is wrong!")
        sys.exit(5)

    if not isfile(args['config_file']):
        log( args['config_file']+" is missing!")
        sys.exit(1)

    config = rsip_ConfigObj( infile=args['config_file'] )
    config.separator = ","  # makes a problem when in INI file.././

    # overwrite ini with command line arguments
    for k in args.keys():
        if args[k] is not None:
            config[k] = args[k]

    config = fix_config(config)



def f():
    return








if __name__=='__main__':
    pass




import time
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
import logging
logging.root.level = 1

if __name__ == "__main__":
    event_handler = LoggingEventHandler()
    observer = Observer()
    observer.schedule(event_handler, path='.', recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
