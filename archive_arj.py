# $f is the filename
# $d is the output dir (defaults to curr-dir). however, program should chdir to desired dir anyway because some doesnt support destination dir.
# $s is the path/file separator (/ or \)
#    win_cmd_list_filter = re.compile('''^ +[0-9]+ +[0-9]+ +[0-9.]+ +[0-9-]+ +[0-9:]+ +[0-9a-z]+ +[a-z-]+ +[BTPMGVX0-9\+]+ +$''', re.I)
#'^[^<>/\:*?"|]+ +[0-9]+ +[0-9]+ +[0-9.]+ +[0-9-]+ +[0-9:]+ +[0-9a-z]+ +[a-z-]+ +[BTPMGVX0-9\+]+ +$'
 
wincmd = 'arj -y x $f $d$s'
wincmd_list = 'arj t $f'
win_cmd_list_filter = re.compile('^Testing(.*?)' + chr(8) + '+', re.M)
lincmd = 'unarj $f'
outtype = 'dir'


#if os.platfrm...
cmd = wincmd_list
cmd = prepare_command(cmd, filename, temp_dir)
#print 'cmd'
#print cmd
#sys.exit()
txt = os.popen(cmd).read()
#print 'txt[:300]'
#print txt[:300]
found_files = win_cmd_list_filter.findall(txt)
found_files = map(strip, found_files) 
#ret = os.system(cmd)
#print 'arj exit status:', found_files

