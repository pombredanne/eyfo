#import file_types
from library import html2text, get_html_headers, err

extensions = ['html', 'htm', ]


def get_contents(f, data):
    #err('html2txt plugin')
    #try:
    html = open(f).read()
    data.titles = get_html_headers(html, flat=True)
    content = unicode( html2text( html ) )
    #content = content.replace('*', ' ')
    data.body = content
    #err('html2txt done len=(%d)' % len(content))
    #except:
    #    pass

    return data

